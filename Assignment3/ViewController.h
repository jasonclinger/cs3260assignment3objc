//
//  ViewController.h
//  Assignment3
//
//  Created by Jason Clinger on 1/28/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "secondViewController.h"

@interface ViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    UITableView* myTableView;
    
    NSArray* array;
}


@end

