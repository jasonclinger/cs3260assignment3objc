//
//  ViewController.m
//  Assignment3
//
//  Created by Jason Clinger on 1/28/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSString* path = [[NSBundle mainBundle]pathForResource:@"PropertyList" ofType:@"plist"];
    array = [[NSArray alloc]initWithContentsOfFile:path];
    
    
    //array = @[@"Test"];
    
    myTableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
    myTableView.delegate = self;
    myTableView.dataSource = self;
    [self.view addSubview: myTableView];
    
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"Recipe Loaded" forKey:@"world"];
    [defaults synchronize];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(reloadData:) name:@"reload" object:nil];
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {return array.count;
}

-(void)reloadData:(NSNotification*)n{
    //get new data from user defaults
    //reload tableview
    NSLog(@"notification recieved...reloading data");
}

//describes what the cell will do and look like, returns and object, UITableViewCell
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell){
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    NSDictionary* dictionary = [array objectAtIndex:indexPath.row];
    //NSDictionary* dictionary = array[indexPath.row];
    //NSString* stringToDispaly = array[indexPath.row];
    
    //NSString* stringToDisplay = dictionary[@"title"];
    //cell.textLabel.text = stringToDisplay;
    cell.textLabel.text = dictionary[@"title"];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self performSegueWithIdentifier:@"rowTouched" sender:indexPath];
    
    //NSDictionary* dictionary = array[indexPath.row];
    
    //NSLog(@"I selected this");
//    secondViewController* svc = [secondViewController new];
//    svc.info = dictionary;
//    
//    [self.navigationController pushViewController:svc animated:YES];
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    NSIndexPath* ip = (NSIndexPath*)sender;
    
    secondViewController* svc = (secondViewController*)segue.destinationViewController;
    svc.info = [array objectAtIndex:ip.row];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
