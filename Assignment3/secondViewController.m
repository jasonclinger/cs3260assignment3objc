//
//  secondViewController.m
//  Assignment3
//
//  Created by Jason Clinger on 1/28/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

#import "secondViewController.h"

@interface secondViewController ()

@end

@implementation secondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor yellowColor];
    
//    UIImageView iv = [[UIImageView alloc]initWithFrame:CGRectMake(100, 100, 100, 100)];
//    iv.image = [UIImage imageNamed:self.info[@"spaghetti"]];
//    iv.contentMode = UIViewContentModeScaleAspectFit;
//    [self.view addSubview:iv];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.title2.text = [self.info objectForKey:@"title"];
    self.imageView.image = [UIImage imageNamed:[self.info objectForKey:@"image"]];
    self.descriptionView.text = [self.info objectForKey:@"description"];
    
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSString* string = [defaults objectForKey:@"world"];
    NSLog(@"%@",string);
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



@end
